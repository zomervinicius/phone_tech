import React, { Component } from "react";
import {
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { UserCard } from "components/UserCard/UserCard.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import avatar from "assets/img/faces/face-3.jpg";

class ScheduleDelivery extends Component {
    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Schedule Delivery"
                                content={
                                    <form>
                                        <div>
                                            <select id="soflow"> <option>Select User...</option>
                                                <option>Yuri Thales da Luz</option>
                                                <option>Julio Raimundo da Mota</option>
                                                <option>Theo Igor Nascimento</option>
                                            </select>
                                        </div>
                                        <div style={{marginTop: '20px', marginBottom: '20px'}}>
                                            <select id="soflow"> <option>Select Phone...</option>
                                                <option>Z3 PLAY</option>
                                                <option>Iphone X</option>
                                                <option>MI A1</option>
                                            </select>
                                        </div>

                                        <FormInputs
                                            ncols={["col-md-6"]}
                                            proprieties={[

                                                {
                                                    label: "Date",
                                                    type: "date",
                                                    bsClass: "form-control",
                                                    placeholder: "Date",
                                                    defaultValue: "12/12/2018"
                                                },
                                            ]}
                                        />
                                        <FormInputs
                                            ncols={["col-md-6"]}
                                            proprieties={[

                                                {
                                                    label: "VALUE",
                                                    type: "Text",
                                                    bsClass: "form-control",
                                                    placeholder: "VALUE",
                                                    defaultValue: "R$50,00"
                                                },
                                            ]}
                                        />
                                        <Row>
                                            <Col md={6}>
                                                <FormGroup controlId="formControlsTextarea">
                                                    <ControlLabel>Description</ControlLabel>
                                                    <FormControl
                                                        rows="5"
                                                        componentClass="textarea"
                                                        bsClass="form-control"
                                                        placeholder="Here can be your description"
                                                        defaultValue="Format Phone."
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>


                                        <Button bsStyle="info" pullCenter fill type="submit">
                                            Schedule Delivery
                    </Button>
                                        <div className="clearfix" />
                                    </form>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default ScheduleDelivery;
