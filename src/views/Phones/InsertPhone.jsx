import React, { Component } from "react";
import {
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { UserCard } from "components/UserCard/UserCard.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import avatar from "assets/img/faces/face-3.jpg";

class InsertPhone extends Component {
    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Insert Phone"
                                content={
                                    <form>
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            proprieties={[

                                                {
                                                    label: "Brand",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "Brand",
                                                    defaultValue: "Motorola"
                                                },
                                                {
                                                    label: "Model",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "Model",
                                                    defaultValue: "Z3 PLAY"
                                                }
                                            ]}
                                        />
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            proprieties={[
                                                {
                                                    label: "phone",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "phone",
                                                    defaultValue: "(47)99999-9999"
                                                },
                                                {
                                                    label: "Operator",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "Operator",
                                                    defaultValue: "TIM"
                                                }
                                            ]}
                                        />                                        
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            proprieties={[
                                                {
                                                    label: "IMEI",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "IMEI",
                                                },
                                                {
                                                    label: "IMEI 2",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "IMEI 2"
                                                }
                                            ]}
                                        />
                                        <div>
                                            <select id="soflow"> <option>Select User...</option>
                                                <option>Yuri Thales da Luz</option>
                                                <option>Julio Raimundo da Mota</option>
                                                <option>Theo Igor Nascimento</option>
                                            </select>
                                        </div>


                                        <Button bsStyle="info" pullRight fill type="submit">
                                            Insert Phone
                    </Button>
                                        <div className="clearfix" />
                                    </form>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
      </div>
        );
    }
}

export default InsertPhone;
