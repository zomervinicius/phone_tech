import UserProfile from "views/UserProfile/UserProfile";
import InsertPhone from "views/Phones/InsertPhone";
import AllUsers from "views/UserProfile/AllUsers";
import AllPhones from "views/Phones/AllPhones";
import ScheduleDelivery from "views/Delivery/ScheduleDelivery";
import AllDeliveries from "views/Delivery/AllDeliveries";

const dashboardRoutes = [
  {
    path: "/insertuser",
    name: "Insert User",
    icon: "pe-7s-user",
    component: UserProfile
  },
  {
    path: "/viewuser",
    name: "View Users",
    icon: "pe-7s-user",
    component: AllUsers
  },
  {
    path: "/insertphone",
    name: "Insert Phone",
    icon: "pe-7s-phone",
    component: InsertPhone
  },
  {
    path: "/viewphone",
    name: "View Phones",
    icon: "pe-7s-phone",
    component: AllPhones
  },
  {
    path: "/scheduledelivery",
    name: "Schedule Delivery",
    icon: "pe-7s-timer",
    component: ScheduleDelivery
  },
  {
    path: "/viewdelivery",
    name: "View Deliveries",
    icon: "pe-7s-timer",
    component: AllDeliveries
  },



  { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
];

export default dashboardRoutes;
